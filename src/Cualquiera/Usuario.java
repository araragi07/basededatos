/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cualquiera;

    //crear las variables o atributos
   
public class Usuario {
     private String nombre;
     private int password;
     private String email;
     private int telefono;

    public String getNombre() {
        return nombre;
    }

    public int getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPassword(int password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
    
     
    
}
